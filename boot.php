<?php

session_start();

define('ROOT', __DIR__);
define('INC', ROOT . '/include');

$config = require ROOT . '/config.php';

require_once INC . '/common.php';
require_once INC . '/database.php';

$flash;
if (isset($_SESSION['flash'])) {
    $flash = $_SESSION['flash'];
    unset($_SESSION['flash']);
}
