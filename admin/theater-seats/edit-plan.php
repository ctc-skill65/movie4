<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/theater-seats/edit-plan.php';

if (!empty($_FILES)) {
    $plan = upload('plan', '/storage/plan');
    if (!$plan) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพผังที่นั้งโรงภาพยนต์ได้");
        redirect($page_path);
    }

    $qr = $db->query("INSERT INTO `theater_plan`(`img`) VALUES ('{$plan}')");
    if ($qr) {
        setAlert('success', "แก้ไขผังที่นั้งโรงภาพยนต์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขผังที่นั้งโรงภาพยนต์ได้");
    }
    redirect($page_path);
}

$data = db_row("SELECT * FROM `theater_plan` ORDER BY `id` DESC");
ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="plan">
        <img src="<?= url($data['img']) ?>" alt="" style="max-width: 55rem;">
    </label>
    <br>
    <label for="plan">แนบภาพผังที่นั้งโรงภาพยนต์</label>
    <input type="file" name="plan" id="plan" accept="image/*" required>
    <br>
    <button type="submit">
        บันทึก
    </button>
</form>


<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขผังที่นั้งโรงภาพยนต์';
require ROOT . '/admin/layout.php';