<?php

require_once __DIR__ . '/../boot.php';

$page_path = '/auth/login.php';

if (!empty($_POST)) {
    $hash = md5($_POST['password']);
    $user = db_row("SELECT * FROM `users` WHERE `email`='{$_POST['email']}' AND `password`='{$hash}'");

    if (empty($user)) {
        setAlert('error', 'อีเมลหรือรหัสผ่านไม่ถูกต้อง');
        redirect($page_path);
    }

    switch ($user['status']) {
        case '1':
            # code...
            break;

        case '-1':
            setAlert('error', 'บัญชีถูกระงับการใช้งาน');
            redirect($page_path);
            break;
        
        default:
            setAlert('error', 'บัญชีไม่มีสิทธิ์เข้าสู่ระบบ');
            redirect($page_path);
            break;
    }

    $_SESSION['user_id'] = $user['user_id'];
    switch ($user['user_type']) {
        case 'user':
            redirect('/user/index.php');
            break;
        
        case 'admin':
            redirect('/admin/index.php');
            break;
    }
}

ob_start();
?>
<h1>เข้าสู่ระบบ</h1>
<h2>ระบบสำรองที่นั่งโรงภาพยนตร์</h2>

<?= showAlert() ?>
<form method="post">
    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" required>
    <label for="password">รหัสผ่าน</label>
    <input type="password" name="password" id="password" required>
    <button type="submit">เข้าสู่ระบบ</button>
</form>

<p>
    คุณยังไม่มีบัญชี? <a href="<?= url('/auth/register.php') ?>">สมัครสมาชิก</a>  
</p>

<?php
$layout_body = ob_get_clean();
require INC . '/base_layout.php';
