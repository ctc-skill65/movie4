<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/users/list.php';

$action = get('action');
$id = get('id');
$sql;
switch ($action) {
    case 'cancel':
        $sql = "UPDATE `users` SET `status`=-1 WHERE `user_id`='$id'";
        break;
    case 'approve':
        $sql = "UPDATE `users` SET `status`=1 WHERE `user_id`='$id'";
        break;
}

if (isset($action)) {
    $db->query($sql);
    redirect($page_path);
}

$items = db_result("SELECT * FROM `users` WHERE `user_type`='user'");

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>อีเมล</th>
            <th>สถานะ</th>
            <th>จัดการบัญชีผู้ใช้งาน</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($items as $item) : ?>
            <tr>
                <td><?= $item['user_id'] ?></td>
                <td><?= $item['firstname'] ?></td>
                <td><?= $item['lastname'] ?></td>
                <td><?= $item['email'] ?></td>
                <td>
                    <?php
                    switch ($item['status']) {
                        case '-1':
                            echo 'ระงับการใช้งาน';
                            break;
                        case '1':
                            echo 'ใช้งาน';
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    switch ($item['status']) {
                        case '-1':
                            ?>
                            <a href="?action=approve&id=<?= $item['user_id'] ?>" <?= clickConfirm("คุณต้องการยกเลิกระงับการใช้งาน {$item['email']} หรือไม่") ?>>ยกเลิกระงับการใช้งาน</a>
                            <?php
                            break;
                        case '1':
                            ?>
                            <a href="?action=cancel&id=<?= $item['user_id'] ?>" <?= clickConfirm("คุณต้องการระงับการใช้งาน {$item['email']} หรือไม่") ?>>ระงับการใช้งาน</a>
                            <?php
                            break;
                    }
                    ?>
                <td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'ข้อมูลผู้ใช้งานระบบ';
require ROOT . '/admin/layout.php';
