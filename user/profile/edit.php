<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = '/user/profile/edit.php';
if (!empty($_POST)) {
    $check = db_result("SELECT * FROM `users` WHERE `email`='{$_POST['email']}' AND `user_id`!='{$user_id}'");
    if (!empty($check)) {
        setAlert('error', "มีอีเมล {$_POST['email']} มีผู้ใช้แล้วไม่สามารถใช้ซ้ำได้");
        redirect($page_path);
    }

    $qr = $db->query("UPDATE `users` SET 
    `firstname`='{$_POST['firstname']}', 
    `lastname`='{$_POST['lastname']}', 
    `email`='{$_POST['email']}' 
    WHERE `user_id`='{$user_id}'");

    if ($qr) {
        setAlert('success', "แก้ไขข้อมูลส่วนตัวสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาดไม่สามารถ แก้ไขข้อมูลส่วนตัวได้");
    }
    redirect($page_path);
}

ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="firstname">ชื่อ</label>
    <input type="text" name="firstname" id="firstname" value="<?= $user['firstname'] ?>" required>
    <label for="lastname">นามสกุล</label>
    <input type="text" name="lastname" id="lastname" value="<?= $user['lastname'] ?>" required>
    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" value="<?= $user['email'] ?>" required> 

    <button type="submit">
        บันทึก
    </button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขข้อมูลส่วนตัว';
require ROOT . '/user/layout.php';