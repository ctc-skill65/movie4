# movie4 3/1/66

# Config file
./config.php

example
```php
<?php

return [
    'site_url' => 'http://skill65.local/movie4',
    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_movie4',
    'db_port' => 3306,
    'db_charset' => 'utf8mb4'
];

```
# Database

./skill65_movie4.sql

admin 
email: admin@demo.com 
password: 12345678 

user 
email: user@demo.com
password: 12345678

