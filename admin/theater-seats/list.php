<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/theater-seats/list.php';

$action = get('action');
$id = get('id');
$sql;

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `theater_seats` WHERE `theater_seat_id`='{$id}'";
        break;
}

if (isset($action)) {
    $db->query($sql);
    redirect($page_path);
}

$items = db_result("SELECT * FROM `theater_seats`");

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อที่นั้ง</th>
            <th>จัดการที่นั้ง</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['theater_seat_id'] ?></td>
                <td><?= $item['seat_name'] ?></td>
                <td>
                    <a href="?action=delete&id=<?= $item['theater_seat_id'] ?>" <?= clickConfirm("คุณต้องการลบที่นั้ง {$item['seat_name']} หรือไม่") ?>>ลบ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'รายการที่นั้งโรงภาพยนต์';
require ROOT . '/admin/layout.php';