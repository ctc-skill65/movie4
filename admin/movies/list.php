<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movies/list.php';

$action = get('action');
$id = get('id');
$sql;

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `movies` WHERE `movie_id`='$id'";
        break;
}

if (isset($action)) {
    $db->query($sql);
    redirect($page_path);
}

$items = db_result("SELECT * FROM `movies`");

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>โปสเตอร์</th>
            <th>ชื่อภาพยนตร์</th>
            <th>จัดการภาพยนตร์</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['movie_id'] ?></td>
                <td>
                    <img src="<?= url($item['poster']) ?>" alt="" style="
                        max-width: 8rem;
                    ">
                </td>
                <td><?= $item['name'] ?></td>
                <td>
                    <a href="<?= url("/admin/movies/edit.php?id={$item['movie_id']}") ?>">แก้ไข</a>
                    <a href="?action=delete&id=<?= $item['movie_id'] ?>" <?= clickConfirm("คุณต้องการลบภาพยนตร์ {$item['name']} หรือไม่") ?>>ลบ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'รายการภาพยนตร์';
require ROOT . '/admin/layout.php';