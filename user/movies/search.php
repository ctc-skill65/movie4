<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$search = get('search');
$search = trim($search);
$items = db_result("SELECT * FROM `movies` WHERE `name` LIKE '%{$search}%'");
ob_start();
?>
<form method="get">
    <label for="search">ค้นหาภาพยนต์</label>
    <input type="search" name="search" id="search" value="<?= $search ?>">
    <button type="submit">
        ค้นหา
    </button>
</form>
<hr>

<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>โปสเตอร์</th>
            <th>ชื่อภาพยนตร์</th>
            <th>รายละเอียดภาพยนตร์</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['movie_id'] ?></td>
                <td>
                    <img src="<?= url($item['poster']) ?>" alt="" style="
                        max-width: 8rem;
                    ">
                </td>
                <td><?= $item['name'] ?></td>
                <td>
                    <a href="<?= url("/user/movies/detail.php?id={$item['movie_id']}") ?>">ดูรายละเอียด</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
$layout_page = ob_get_clean();
$page_name = 'ค้นหาภาพยนต์';
require ROOT . '/user/layout.php';
