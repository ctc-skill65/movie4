<?php

require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movies/add.php';

if (!empty($_POST)) {
    $poster = upload('poster', '/storage/poster');
    if (!$poster) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพโปสเตอร์ภาพยนตร์ได้");
        redirect($page_path);
    }
    // var_dump($poster);
    // exit;
    $qr = $db->query("INSERT INTO `movies`(
    `name`, 
    `poster`) 
    VALUES (
    '{$_POST['name']}',
    '{$poster}')");
    if ($qr) {
        setAlert('success', "เพิ่มภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มภาพยนตร์ได้");
    }
    redirect($page_path);
}

ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="poster">แนบภาพโปสเตอร์ภาพยนตร์</label>
    <input type="file" name="poster" id="poster" accept="image/*" required>
    <label for="name">ชื่อภาพยนตร์</label>
    <input type="text" name="name" id="name" required>
    <button type="submit">
        บันทึก
    </button>
</form>


<?php
$layout_page = ob_get_clean();
$page_name = 'เพิ่มภาพยนตร์';
require ROOT . '/admin/layout.php';