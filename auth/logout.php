<?php

require_once __DIR__ . '/../boot.php';

session_destroy();
session_start();

setAlert('success', 'คุณออกจากระบบแล้ว');
redirect('/auth/login.php');
