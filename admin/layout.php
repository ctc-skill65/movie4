<?php
ob_start();
?>
<h1><?= isset($page_name) ? $page_name : null ?></h1>
<p>
    <?= $user['firstname'] . ' ' . $user['lastname'] ?> (ผู้ดูแลระบบ) 
</p>
<nav>
    <ul>
        <li><a href="<?= url('/admin/index.php') ?>">หน้าหลัก</a></li>
        <li>จัดการข้อมูลผู้ใช้งานระบบ
            <ul>
                <li><a href="<?= url('/admin/users/list.php') ?>">ข้อมูลผู้ใช้งานระบบ</a></li>
            </ul>
        </li>
        <li>จัดการภาพยนต์
            <ul>
                <li><a href="<?= url('/admin/movies/add.php') ?>">เพิ่มภาพยนตร์</a></li>
                <li><a href="<?= url('/admin/movies/list.php') ?>">รายการภาพยนตร์</a></li>
            </ul>
        </li>
        <li>จัดการเวลาฉายภาพยนต์
            <ul>
                <li><a href="<?= url('/admin/movie-times/add.php') ?>">เพื่มเวลาฉายภาพยนต์</a></li>
                <li><a href="<?= url('/admin/movie-times/list.php') ?>">รายการเวลาฉายภาพยนต์</a></li>
            </ul>
        </li>
        <li>จัดการที่นั้งโรงภาพยนต์
            <ul> 
                <li><a href="<?= url('/admin/theater-seats/edit-plan.php') ?>">แก้ไขผังที่นั้งโรงภาพยนต์</a></li>
                <li><a href="<?= url('/admin/theater-seats/add.php') ?>">เพิ่มที่นั้งโรงภาพยนต์</a></li>
                <li><a href="<?= url('/admin/theater-seats/list.php') ?>">รายการที่นั้งโรงภาพยนต์</a></li>
            </ul>
        </li>
        <li>ข้อมูลส่วนตัว
            <ul>
                <li><a href="<?= url('/admin/profile/edit.php') ?>">แก้ไขข้อมูลส่วนตัว</a></li>
                <li><a href="<?= url('/admin/profile/edit-pass.php') ?>">แก้ไขรหัสผ่าน</a></li>
                <li><a href="<?= url('/auth/logout.php') ?>">ออกจากระบบ</a></li>
            </ul>
        </li>
    </ul>
</nav>

<main>
<?= isset($layout_page) ? $layout_page : null ?>
</main>

<?php
$layout_body = ob_get_clean();
if (isset($page_name))
    $layout_title = $page_name;
require INC . '/base_layout.php';
