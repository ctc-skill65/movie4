<?php

return [
    'site_url' => 'http://skill65.local/movie4',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_movie4',
    'db_port' => '3306', 
    'db_charset' => 'utf8mb4'
];
